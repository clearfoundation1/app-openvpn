<?php

$lang['openvpn_app_description'] = 'Додаток OpenVPN забезпечує безпечний віддалений доступ до цієї системи та вашої локальної мережі.';
$lang['openvpn_app_name'] = 'OpenVPN';
$lang['openvpn_auto_configure_help'] = 'Сервер OpenVPN зараз перебуває в режимі автоматичного налаштування. Якщо ви зміните налаштування мережі, OpenVPN інтелектуально внесе необхідні зміни в свої налаштування. Якщо ви хочете вимкнути автоматичне налаштування, натисніть наведену нижче кнопку.';
$lang['openvpn_internet_hostname_tooltip'] = 'Інтернет-хост використовується в конфігурації клієнта OpenVPN, яка надається кінцевим користувачам. Це ім’я хоста можна змінити в додатку Настройка IP.';
$lang['openvpn_linux'] = 'Linux';
$lang['openvpn_mac_os'] = 'MacOS';
$lang['openvpn_windows'] = 'Windows';

$lang['openvpn_additional_settings'] = 'Додаткові налаштування';
$lang['openvpn_nat_enabled'] = 'Увімкнути NAT (лише в режимі шлюзу)';
$lang['openvpn_redirect_gateway'] = 'Примусово пересилати весь трафік через VPN';
$lang['openvpn_client_to_client'] = 'Дозволити трафік від клієнта до клієнта';
$lang['openvpn_block_outside_dns'] = 'Примусове використання DNS через VPN';
